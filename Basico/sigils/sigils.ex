iex(22)> ~c/2 + 7 = #{2 + 7}/
'2 + 7 = 9'
iex(23)> ~C/2 + 7 = #{2 + 7}/
'2 + 7 = \#{2 + 7}'
iex(24)> "minha" <> _resto = "minha string"
"minha string"
iex(25)> <<"minha"::utf8, _resto>> = "minha string"
** (MatchError) no match of right hand side value: "minha string"

iex(25)> for <<c <- "hello">>, do: <<c>>
["h", "e", "l", "l", "o"]
iex(26)> i "hello"
Term
  "hello"
Data type
  BitString
Byte size
  5
Description
  This is a string: a UTF-8 encoded binary. It's printed surrounded by
  "double quotes" because all UTF-8 encoded code points in it are printable.
Raw representation
  <<104, 101, 108, 108, 111>>
Reference modules
  String, :binary
Implemented protocols
  Collectable, IEx.Info, Inspect, List.Chars, String.Chars
iex(27)> for {k, v} <- [one: 1, two: 2, three: 3], into: %{}, do: {k, v}
%{one: 1, three: 3, two: 2}
