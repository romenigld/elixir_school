defmodule Greeter do
  def hello(names, language_code \\ "en")

  def hello(names, language_code) when is_list(names) do
    names
    |> Enum.join(", ")
    |> hello(language_code)
    |> IO.puts
  end

  def hello(name, language_code) when is_binary(name) do
    phrase(language_code) <> name
  end

  defp phrase("en"), do: "Hello, "
  defp phrase("pt"), do: "Olá, "
end

Greeter.hello ["Sean", "Steve"]
#=> "Hello, Sean, Steve"

Greeter.hello ["Sean", "Steve"], "pt"
#=> "Olá, Sean, Steve"
