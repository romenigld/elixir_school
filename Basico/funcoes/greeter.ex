defmodule Greeter do
  def hello(names, lang_code \\ "en")

  def hello(names, lang_code) when is_list(names) do
    names
    |> Enum.join(", ")
    |> phrase(lang_code)
    |> IO.puts
  end

  defp phrase(name, "en") when is_binary(name), do: " Hello " <> name
  defp phrase(name, "pt") when is_binary(name), do: " Olá " <> name
end

names = ["Romenig", "Leli"]

Greeter.hello(names)
Greeter.hello(names, "en")
Greeter.hello(names, "pt")
